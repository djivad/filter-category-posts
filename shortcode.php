<?php
/**
 * Posts by Category
 *
 * @package           Filter Category Posts
 * @author            David Riviera
 * @copyright         2021 David Riviera
 * @license           GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name:       Filter Category Posts
 * Plugin URI:        https://www.driviera.com/
 * Description:       Displays 3 most recent posts of a category. Use [filtercategoryposts catid="#"].
 * Version:           0.0.1
 * Requires at least: 5.7
 * Requires PHP:      7.4
 * Author:            David Riviera
 * Author URI:        https://www.driviera.com/
 * Text Domain:       filtercategoryposts
 * License:           GPL v2 or later
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 */

/*
 * Add [postsbycategory] shortcode
 */
function fcp_filtercategoryposts( $atts = [], $content = null, $tag = '' ) {
	// the parameters
		// normalize attribute keys, lowercase
		$atts = array_change_key_case( (array) $atts, CASE_LOWER );

		// override default attributes with user attributes
		$fcp_atts = shortcode_atts(
			array(
				'catid' => '1',
			), $atts, $tag
		);

	// the query
	$the_query = new WP_Query( array( 'cat' => $fcp_atts['catid'], 'posts_per_page' => 3 ) ); 

	// The Loop
		$string = ''; //Initalize $string
	if ( $the_query->have_posts() ) {
		$string .= '<div class="filtercategoryposts widget_recent_entries">';
		while ( $the_query->have_posts() ) {
			$the_query->the_post();
				$string .= '<div class="outer">'; // outer div
				$string .= '<div class="inner-1">'; // first inner div
					if ( has_post_thumbnail() ) {
						$string .= sprintf( '<a class="post-thumbnail" href="%1$s" rel="bookmark"><img src="%2$s" alt="%3$s"></a>', get_the_permalink(), get_the_post_thumbnail_url( $the_query->ID, 'square-small' ), esc_html( get_the_title() ) );
				} else { 
					// if no featured image is found do nothing
				}
				$string .= '</div>'; // close first inner div
				$string .= '<div class="inner-2">'; // second inner div
					$string .= '<a href="' . get_the_permalink() .'" rel="bookmark">' . get_the_title() .'</a>';
				$string .= '</div>'; // close second inner div
				$string .= '</div>'; // close outer div
				}
			$thiscat = get_category( $fcp_atts['catid'] );
			$string .= sprintf( '<a href="/category/%1$s/%2$s">Read more about %3$s</a>', $thiscat->slug, $thiscat->slug, $thiscat->name);
		} else {
				$string .= '<p>No items found.</p>';
		}

		$string .= '</div>';
		return $string;

	/* Restore original Post Data */
	wp_reset_postdata();
}
// Add a shortcode
add_shortcode('filtercategoryposts', 'fcp_filtercategoryposts');
 
// Enable shortcodes in text widgets
add_filter('widget_text', 'do_shortcode');
