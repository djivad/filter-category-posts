# README #

The *Filter Category Posts* plugin displays the three most recent posts in a given category. This plugin is a conceptual work-in-progress.

### What is this repository for? ###

* Currently, this plugin is for personal use only.
* Version 0.0.1
* [Meet David](https://driviera.com/)

### How do I get set up? ###

* Installation
	* Use [this git repository](https://bitbucket.org/djivad/filter-category-posts/)
	* Activate the plugin in the WordPress Dashboard > Installed Plugins page
* Configuration
	* Simply place the [filterbycategory catid="#"] shortcode where you'd like the posts to appear, such as a Text Widget
* Dependencies
	* None
* Database configuration
	* None
* How to run tests
	* Testing involves entering the [filterbycategory catid="#"] in a text location with shortcodes enabled
	* If your posts appear in place of the shortcode, the plugin is working
* Deployment instructions
	* Ensure the *<wordpress_root>/wp-content/plugins/filterbycategory* is included in your deployment plan

### Who do I talk to? ###

* Repo owner: [David Riviera](https://www.driviera.com/)